
/**
 * Comprueba si el número proporcionado es válido.
 *
 * @param {number} diceCount - El número a comprobar.
 * @returns {boolean} - Devuelve true si el número es válido, false en caso contrario.
 */
function checkValues(diceCount) {
    let resultsContainer = document.getElementById("results");
    while (resultsContainer.firstChild) {
        resultsContainer.removeChild(resultsContainer.firstChild);
    }
    if (!diceCount) {
        let errorMessage = document.createTextNode("Por favor, introduce un número");
        let errorParagraph = document.createElement("p");
        errorParagraph.style.color = "red";
        errorParagraph.appendChild(errorMessage);
        resultsContainer.appendChild(errorParagraph);
        document.getElementById("number").focus();
        return false;
    }
    if (diceCount <= 0 || diceCount > 50) {
        let errorMessage = document.createTextNode("Por favor, introduce un número entre 1 y 50");
        let errorParagraph = document.createElement("p");
        errorParagraph.style.color = "red";
        errorParagraph.appendChild(errorMessage);
        resultsContainer.appendChild(errorParagraph);
        document.getElementById("number").focus();
        return false;
    }
    return true;
}

/**
 * Genera un array de dados aleatorios.
 *
 * @param {number} numberOfDice - El número de dados a generar.
 * @returns {number[]} - Devuelve un array con los valores de los dados generados.
 */
function generateDice(numberOfDice) {
    let diceValuesArray = [];
    for (let pos = 0; pos < numberOfDice; pos++) {
        diceValuesArray.push(Math.floor(Math.random() * 6) + 1);
    }
    return diceValuesArray;
}

/**
 * Muestra los dados en la página.
 *
 * @param {number[]} dice - El array de valores de los dados.
 */
function showDice(dice) {
    let resultsDiv = document.getElementById("results");
    resultsDiv.innerHTML = ""; // Limpia
    for (let pos = 0; pos < dice.length; pos++) {
        const diceImg = document.createElement("img");
        diceImg.src = `./assets/img/Dice_${dice[pos]}.png`;
        resultsDiv.appendChild(diceImg);
    }
}

/**
 * Muestra los números de los dados en la página.
 *
 * @param {number[]} dice - El array de valores de los dados.
 */
function showNumbers(dice) {
    let resultsDiv = document.getElementById("results");
    let numbersDiv = document.createElement("div");
    numbersDiv.innerHTML = "<h2>Resultados</h2><p>Los valores son: " + dice.join(" ") + "</p>";
    resultsDiv.appendChild(numbersDiv);
}


/**
 * Muestra el total de los valores de los dados en la página.
 *
 * @param {number[]} dice - El array de valores de los dados.
 */
function showTotal(dice) {
    let resultsDiv = document.getElementById("results");
    let totalDiv = document.createElement("div");
    let total = 0;
    for (let pos = 0; pos < dice.length; pos++) {
        total += dice[pos];
    }
    totalDiv.innerHTML = "<p>Total: " + total + "</p>";
    resultsDiv.appendChild(totalDiv);
}

/**
 * Función principal que se ejecuta al lanzar los dados.
 */
function rollDice() {
    let number = parseInt(document.getElementById("number").value);
    if (!checkValues(number)) {
        return;
    }
    let dice = generateDice(number);
    showDice(dice);
    showNumbers(dice);
    showTotal(dice);
}
